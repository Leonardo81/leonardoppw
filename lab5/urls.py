from django.contrib import admin
from django.urls import path
from . import views

app_name='lab5'
urlpatterns = [
    path('create', views.create_jadwal),
    path('', views.jadwal, name='jadwal'),
    path('delete', views.delete)
]
