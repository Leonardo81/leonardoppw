from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.

def index(request):
    return render(request, "lab3/myProfile.html")

def game(request):
    return render(request, "lab3/game.html")

def ping(request):
    return HttpResponse("pong")