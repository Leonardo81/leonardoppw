from django.contrib import admin
from django.urls import path
from . import views

app_name='lab3'
urlpatterns = [
    path('', views.index, name='lab3'),
    path('game', views.game, name='game'),
    path('ping/', views.ping, name='ping')
]
