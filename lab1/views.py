from django.shortcuts import render
# from django.http import HttpResponse

# Create your views here.

def profile_home(request):
    return render(request, 'lab1/index.html')

def index(request):
    return render(request, 'lab1/landing.html')