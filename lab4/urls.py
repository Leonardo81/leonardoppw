from django.contrib import admin
from django.urls import path
from . import views

app_name='lab4'
urlpatterns = [
    path('', views.index),
    path('about', views.about),
    path('experience', views.experience),
    path('skill', views.skill),
    path('contact', views.contact),
    path('game', views.game),
]
