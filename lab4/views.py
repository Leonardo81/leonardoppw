from django.shortcuts import render
# Create your views here.

def index(request):
    return render(request, "lab4/landing.html")

def about(request):
    return render(request, "lab4/about.html")

def experience(request):
    return render(request, "lab4/experience.html")

def skill(request):
    return render(request, "lab4/skill.html")

def contact(request):
    return render(request, "lab4/contact.html")  

def game(request):
    return render(request, "lab4/game.html")